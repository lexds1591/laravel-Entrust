<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRbacGroupRolePermissionsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('RbacGroupRolePermissions', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('groupId')->unsigned()->nullable(false);
            $table->integer('roleId')->unsigned()->nullable(false);
            $table->integer('permissionId')->unsigned()->nullable(false);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('groupId')->references('id')->on('RbacGroups');
            $table->foreign('roleId')->references('id')->on('RbacRoles');
            $table->foreign('permissionId')->references('id')->on('RbacPermissions');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('RbacGroupRolePermissions');
	}

}
