<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRbacUserPrivilegesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('RbacUserPrivileges', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('userId')->unsigned()->nullable(false);
            $table->integer('groupId')->unsigned()->nullable(false);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('userId')->references('id')->on('users');
            $table->foreign('groupId')->references('id')->on('RbacGroups');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('RbacUserPrivileges');
	}

}
