<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlatformUsersTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('PlatformUsers', function(Blueprint $table) {
            $table->integer('id')->unsigned()->nullable(false);
            $table->string('userName', 100)->nullable(false);
            $table->string('firstName', 100)->nullable();
            $table->string('lastName', 100)->nullable();
            $table->string('email', 100)->nullable();
            $table->boolean('ibNetworkUser')->nullable(false);
            $table->boolean('isAdmin')->nullable(false);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('PlatformUsers');
	}

}
