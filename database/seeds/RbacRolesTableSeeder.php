<?php

use Illuminate\Database\Seeder;
use App\Models\RbacRole;

class RbacRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(RbacRole::class, 1)->create(['name' => 'Providers-Menu']);
        factory(RbacRole::class, 1)->create(['name' => 'Leads-Menu']);
        factory(RbacRole::class, 1)->create(['name' => 'CM-Menu']);
        factory(RbacRole::class, 1)->create(['name' => 'Financial-Menu']);
        factory(RbacRole::class, 1)->create(['name' => 'Affiliates-Menu']);
        factory(RbacRole::class, 1)->create(['name' => 'Admin-Menu']);
        factory(RbacRole::class, 1)->create(['name' => 'Admin-Main']);
        factory(RbacRole::class, 1)->create(['name' => 'Create-User']);
        factory(RbacRole::class, 1)->create(['name' => 'Edit-User']);
        factory(RbacRole::class, 1)->create(['name' => 'Create-Group']);
        factory(RbacRole::class, 1)->create(['name' => 'Edit-Group']);
        factory(RbacRole::class, 1)->create(['name' => 'Group-Security']);
        factory(RbacRole::class, 1)->create(['name' => 'User-Privileges']);
    }
}
