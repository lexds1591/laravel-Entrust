<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(PlatformUsersTableSeeder::class);
        $this->call(RbacPermissionsTableSeeder::class);
        $this->call(RbacRolesTableSeeder::class);
        $this->call(RbacGroupsTableSeeder::class);
        $this->call(RbacGroupRolePermissionsTableSeeder::class);
        $this->call(RbacUserPrivilegesTableSeeder::class);
    }
}
