<?php

use Illuminate\Database\Seeder;
use App\Models\PlatformUser;

class PlatformUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(PlatformUser::class, 1)->states('admin')->create();
        factory(PlatformUser::class, 1)->states('power_user')->create();
    }
}
