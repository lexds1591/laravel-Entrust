<?php

use Illuminate\Database\Seeder;
use App\Models\RbacGroup;

class RbacGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(RbacGroup::class, 1)->create(['name' => 'PowerUsers']);
    }
}
