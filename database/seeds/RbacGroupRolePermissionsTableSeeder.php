<?php

use Illuminate\Database\Seeder;
use App\Models\RbacGroupRolePermission;
use App\Models\RbacRole;

class RbacGroupRolePermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(RbacGroupRolePermission::class, 1)->create([
            'roleId' => RbacRole::where('name', 'Providers-Menu')->first()->id
        ]);

        factory(RbacGroupRolePermission::class, 1)->create([
            'roleId' => RbacRole::where('name', 'Leads-Menu')->first()->id
        ]);

        factory(RbacGroupRolePermission::class, 1)->create([
            'roleId' => RbacRole::where('name', 'Financial-Menu')->first()->id
        ]);

        factory(RbacGroupRolePermission::class, 1)->create([
            'roleId' => RbacRole::where('name', 'CM-Menu')->first()->id
        ]);

        factory(RbacGroupRolePermission::class, 1)->create([
            'roleId' => RbacRole::where('name', 'Affiliates-Menu')->first()->id
        ]);
    }
}
