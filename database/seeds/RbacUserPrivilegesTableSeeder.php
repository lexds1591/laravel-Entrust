<?php

use Illuminate\Database\Seeder;
use App\Models\RbacUserPrivilege;
use App\Models\RbacGroup;
use App\User;

class RbacUserPrivilegesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rbacGroup = RbacGroup::all();

        $rbacGroup->each(function($group){
            factory(RbacUserPrivilege::class, 1)->create([
                'userId' => User::where('name', 'power user')->first()->id,
                'groupId' => $group->id
            ]);
        });

    }
}
