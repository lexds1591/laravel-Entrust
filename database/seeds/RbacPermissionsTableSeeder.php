<?php

use Illuminate\Database\Seeder;
use App\Models\RbacPermission;

class RbacPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(RbacPermission::class, 1)->create(['name' => 'View']);
        factory(RbacPermission::class, 1)->create(['name' => 'Create']);
        factory(RbacPermission::class, 1)->create(['name' => 'Edit']);
        factory(RbacPermission::class, 1)->create(['name' => 'Delete']);
    }
}
