<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\PlatformUser::class, function (Faker $faker) {
    return [
        'userName' => $faker->userName,
        'firstName' => $faker->firstName,
        'lastName' => $faker->lastName,
        'ibNetworkUser' => false,
        'isAdmin' => false,
        'email' => $faker->unique()->safeEmail,
    ];
});

$factory->state(App\Models\PlatformUser::class, 'admin', function (Faker $faker) {
    return [
        'id' => \App\User::where('name', 'admin user')->first()->id,
        'userName' => 'admin',
        'firstName' => 'admin',
        'lastName' => 'user',
        'ibNetworkUser' => false,
        'isAdmin' => true,
        'email' => 'admin@nolo.com',
    ];
});

$factory->state(App\Models\PlatformUser::class, 'power_user', function (Faker $faker) {
    return [
        'id' => \App\User::where('name', 'power user')->first()->id,
        'userName' => 'power',
        'firstName' => 'power',
        'lastName' => 'user',
        'ibNetworkUser' => false,
        'isAdmin' => false,
        'email' => 'power-user@nolo.com',
    ];
});