<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\RbacGroupRolePermission::class, function (Faker $faker) {
    return [
        'groupId' => App\Models\RbacGroup::where('name', 'PowerUsers')->first()->id,
        'roleId' => 1,
        'permissionId' => 1,
    ];
});
