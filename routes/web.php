<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    if (!Auth::guest()) {
        return redirect('/home');
    }

    return redirect('/login');
});


/**
 * Testing NoloAuthMiddleware with this route
 * Modify to send to params into the middleware: role and permissions
 */
Route::get('/home', 'HomeController@index')
    ->name('home');

Route::group(['prefix' => 'admin', 'middleware' => 'permissions:Admin-Menu,View'], function () {
    Route::get('/', 'AdminController@index')
        ->name('admin.index');

    Route::group(['prefix' => 'user'], function () {
        Route::get('/create', 'UserController@create')
            ->name('admin.user.create')
            ->middleware('permissions:Create-User,View');
        Route::post('/store', 'UserController@store')
            ->name('admin.user.store')
            ->middleware('permissions:Create-User,Create');

        Route::get('/edit/{id}', 'UserController@edit')
            ->name('admin.user.edit')
            ->middleware('permissions:Edit-User,View');
        Route::put('/update/{id}', 'UserController@update')
            ->name('admin.user.update')
            ->middleware('permissions:Edit-User,Edit');
        Route::delete('/delete/{id}', 'UserController@delete')
            ->name('admin.user.delete')
            ->middleware('permissions:Edit-User,Delete');
    });

    Route::group(['prefix' => 'group'], function () {
        Route::get('/create', 'RbacGroupsController@create')
            ->name('admin.group.create')
            ->middleware('permissions:Create-Group,View');
        Route::post('/store', 'RbacGroupsController@store')
            ->name('admin.group.store')
            ->middleware('permissions:Create-Group,Create');

        Route::get('/edit/{id}', 'RbacGroupsController@edit')
            ->name('admin.group.edit')
            ->middleware('permissions:Edit-Group,View');
        Route::put('/update/{id}', 'RbacGroupsController@update')
            ->name('admin.group.update')
            ->middleware('permissions:Edit-Group,Edit');
        Route::delete('/delete/{id}', 'RbacGroupsController@delete')
            ->name('admin.group.delete')
            ->middleware('permissions:Edit-Group,Delete');
    });

    Route::group(['prefix' => 'groupsecurity'], function (){
       Route::get('/', 'RbacGroupRolePermissionsController@index')
           ->name('admin.grouprolepermission.index')
           ->middleware('permissions:Group-Security,View');
       Route::post('/store', 'RbacGroupRolePermissionsController@store')
           ->name('admin.grouprolepermission.store')
           ->middleware('permissions:Group-Security,Create');
       Route::delete('/delete/{id}', 'RbacGroupRolePermissionsController@destroy')
           ->name('admin.grouprolepermission.delete')
           ->middleware('permissions:Group-Security,Delete');
    });

    Route::group(['prefix' => 'userprivileges'], function (){
        Route::get('/', 'RbacUserPrivilegesController@index')
            ->name('admin.userprivileges.index')
            ->middleware('permissions:User-Privileges,View');
        Route::post('/store', 'RbacUserPrivilegesController@store')
            ->name('admin.userprivileges.store')
            ->middleware('permissions:User-Privileges,Create');
        Route::delete('/delete/{id}', 'RbacUserPrivilegesController@destroy')
            ->name('admin.userprivileges.delete')
            ->middleware('permissions:User-Privileges,Delete');
    });

});