

@extends('layouts.app')

@section('content')
    <div class="container">
        @if(session()->has('message'))
            <div class="alert alert-success alert-dismissible fade in">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Group Security
                    </div>
                    <div class="panel-body">
                        <form method="POST" action="{{route('admin.grouprolepermission.store')}}" class="form-inline">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="groupId" class="col-md-5">
                                    Groups: <span class="text-danger">*</span>
                                </label>
                                <div class="col-md-5">
                                    <select name="groupId" id="groupId" class="form-control" required>
                                        <option value="">Select group</option>
                                        @foreach($groups as $key => $group)
                                            <option value="{{ $group['id'] }}">{{ $group['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="roleId" class="col-md-5">
                                    Roles: <span class="text-danger">*</span>
                                    <span data-toggle="tooltip" title="You can select one or multiple roles">
                                        <i class="glyphicon glyphicon-question-sign">
                                        </i>
                                    </span>
                                </label>
                                <div class="col-md-5">
                                    <select name="roleId[]" id="roleId" class="form-control" multiple="multiple" required>
                                        @foreach($roles as $key => $role)
                                            <option value="{{ $role['id'] }}">{{ $role['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="permissionId" class="col-md-6">
                                    Permissions: <span class="text-danger">*</span>
                                </label>
                                <div class="col-md-5">
                                    <select name="permissionId[]" id="permissionId" class="form-control" multiple="multiple" disabled="disabled" required>
                                        @foreach($permissions as $key => $permission)
                                            <option value="{{ $permission['id'] }}" {{ ($permission['id'] =='1') ? 'selected' : '' }}>{{ $permission['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary col-md-offset-1">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Group Security List
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Group</th>
                                    <th>Role</th>
                                    <th>Permission</th>
                                    <th>Actions</th>
                                </tr>

                            </thead>
                            <tbody>
                                @foreach($groupRolePermissionList as $key => $grp)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $grp['group']['name'] }}</td>
                                        <td>{{ $grp['role']['name'] }}</td>
                                        <td>{{ $grp['permission']['name'] }}</td>
                                        <td>
                                            <form method="POST" action="{{route('admin.grouprolepermission.delete',
                                            ['id' => $grp['id']])}}">
                                                {{ csrf_field() }}
                                                {{ method_field("DELETE") }}
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection