
@extends('layouts.app')

@section('content')
    <div class="container">
        @if(session()->has('message'))
            <div class="alert alert-success alert-dismissible fade in">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                @canAccess('User-Privileges', ['Create'])
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            User Privileges
                        </div>
                        <div class="panel-body">
                            <form method="POST" action="{{route('admin.userprivileges.store')}}" class="form-inline">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="roleId" class="col-md-5">
                                        Users: <span class="text-danger">*</span>
                                    </label>
                                    <div class="col-md-5">
                                        <select name="userId" id="userId" class="form-control" required>
                                            <option value="">Select User</option>
                                            @foreach($users as $key => $user)
                                                <option value="{{ $user->id }}">{{ $user->firstName . ' ' . $user->lastName }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="groupId" class="col-md-5">
                                        Groups: <span class="text-danger">*</span>
                                    </label>
                                    <div class="col-md-5">
                                        <select name="groupId" id="groupId" class="form-control" required>
                                            <option value="">Select Group</option>
                                            @foreach($groups as $key => $group)
                                                <option value="{{ $group->id }}">
                                                    {{ $group->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary col-md-offset-1">Submit</button>
                            </form>
                        </div>
                    </div>
                @endcanAccess
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        User Privilege List
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>User</th>
                                    <th>Group</th>
                                    <th>Actions</th>
                                </tr>

                            </thead>
                            <tbody>
                                @foreach($userPrivilegeList as $key => $upl)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $upl->platformUser->firstName . ' ' .
                                               $upl->platformUser->lastName }}
                                        </td>
                                        <td>{{ $upl->group->name }}</td>
                                        <td>
                                            @canAccess('User-Privileges', ['Delete'])
                                            <form method="POST" action="{{ route('admin.userprivileges.delete', ['id' => $upl->id]) }}">
                                                {{ csrf_field() }}
                                                {{ method_field("DELETE") }}
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                            @endcanAccess
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection