@extends('layouts.app')

@section('content')
<div class="container user-component">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @if($isCreate)
                        Create
                    @else
                        Edit
                    @endif
                    User
                    @if($isEdit)
                        @canAccess('Edit-User', ['Delete'])
                            <div class="pull-right">
                                <form method="POST" action="{{$deleteRoute}}">
                                    {{ csrf_field() }}
                                    {{ method_field("DELETE") }}
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </div>
                        @endcanAccess
                    @endif
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{$action}}" class="form-horizontal">
                        {{ csrf_field() }}
                        @if($isEdit)
                            {{ method_field("PUT") }}
                        @endif
                        <div class="form-group">
                            <label for="firstName" class="col-md-2 col-md-offset-1">
                                First Name: <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-5">
                                <input
                                    type="text"
                                    id="firstName"
                                    name="firstName"
                                    class="form-control"
                                    value="{{($isEdit) ? $platformUser->firstName : ''}}"
                                    required >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lastName" class="col-md-2 col-md-offset-1">
                                Last Name: <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-5">
                                <input
                                    type="text"
                                    id="lastName"
                                    name="lastName"
                                    class="form-control"
                                    value="{{($isEdit) ? $platformUser->lastName : ''}}"
                                    required >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="userName" class="col-md-2 col-md-offset-1">
                                UserName: <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-5">
                                <input
                                    type="text"
                                    id="userName"
                                    name="userName"
                                    class="form-control"
                                    value="{{($isEdit) ? $platformUser->userName : ''}}"
                                    required >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ibNetworkUser" class="col-md-2 col-md-offset-1">
                                Ib Network User:
                            </label>
                            <div class="col-md-5">
                                <input
                                    type="checkbox"
                                    id="ibNetworkUser"
                                    name="ibNetworkUser"
                                    {{($isEdit && $platformUser->ibNetworkUser) ? 'checked' : ''}}
                                    class="form-control" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="isAdmin" class="col-md-2 col-md-offset-1">
                                Admin:
                            </label>
                            <div class="col-md-5">
                                <input
                                        type="checkbox"
                                        id="isAdmin"
                                        name="isAdmin"
                                        {{($isEdit && $platformUser->isAdmin) ? 'checked' : ''}}
                                class="form-control" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-2 col-md-offset-1">
                                Email: <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-5">
                                <input
                                    type="email"
                                    id="email"
                                    name="email"
                                    class="form-control"
                                    value="{{($isEdit) ? $platformUser->email : ''}}"
                                    required >
                            </div>
                        </div>
                        <div class="password-container" style="{{($isEdit && $platformUser->ibNetworkUser)? 'display:none;' : ''}}">
                            <div class="form-group">
                                <label for="password" class="col-md-2 col-md-offset-1">
                                    Password:
                                    @if($isCreate)
                                        <span class="text-danger">*</span>
                                    @endif
                                </label>
                                <div class="col-md-5">
                                    <input
                                        type="password"
                                        id="password"
                                        name="password"
                                        class="form-control"
                                        {{($isCreate) ? 'required' : '' }}>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password_confirmation" class="col-md-2 col-md-offset-1">
                                    Confirm Password:
                                    @if($isCreate)
                                        <span class="text-danger">*</span>
                                    @endif
                                </label>
                                <div class="col-md-5">
                                    <input
                                        type="password"
                                        id="password_confirmation"
                                        name="password_confirmation"
                                        class="form-control"
                                        {{($isCreate) ? 'required' : '' }}>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary col-md-6 col-md-offset-2">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
