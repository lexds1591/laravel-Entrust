@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @if($isCreate)
                        Create
                    @else
                        Edit
                    @endif
                    Group
                    @if($isEdit)
                        @canAccess('Edit-User', ['Delete'])
                            <div class="pull-right">
                                <form method="POST" action="{{$deleteRoute}}">
                                    {{ csrf_field() }}
                                    {{ method_field("DELETE") }}
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </div>
                        @endcanAccess
                    @endif
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{$action}}" class="form-horizontal">
                        {{ csrf_field() }}
                        @if($isEdit)
                            {{ method_field("PUT") }}
                        @endif
                        <div class="form-group">
                            <label for="name" class="col-md-2 col-md-offset-1">
                                Name: <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-5">
                                <input
                                    type="text"
                                    id="name"
                                    name="name"
                                    class="form-control"
                                    value="{{($isEdit) ? $group->name : ''}}"
                                    required >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="notes" class="col-md-2 col-md-offset-1">
                                Notes: <span class="text-danger">*</span>
                            </label>
                            <div class="col-md-5">
                                <textarea
                                    type="text"
                                    id="notes"
                                    name="notes"
                                    class="form-control"
                                    required >{{($isEdit) ? $group->notes : ''}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary col-md-6 col-md-offset-2">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
