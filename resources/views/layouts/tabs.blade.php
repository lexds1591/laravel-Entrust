@if (!Auth::guest())
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{route('home')}}">Home</a></li>
                    @canAccess('Admin-Menu', ['View'])
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                Admin<i class="caret"></i>
                            </a>
                            <ul class="dropdown-menu">
                                @canAccess('Admin-Main', ['View'])
                                    <li><a href="{{route('admin.index')}}">Main</a></li>
                                @endcanAccess
                                @canAccess('Create-User', ['View'])
                                    <li><a href="{{route('admin.user.create')}}">Create User</a></li>
                                @endcanAccess
                                @canAccess('Create-Group', ['View'])
                                    <li><a href="{{route('admin.group.create')}}">Create Group</a></li>
                                @endcanAccess
                                @canAccess('Group-Security', ['View'])
                                    <li><a href="{{route('admin.grouprolepermission.index')}}">Group Security</a></li>
                                @endcanAccess
                                @canAccess('User-Privileges', ['View'])
                                    <li><a href="{{route('admin.userprivileges.index')}}">User Privileges</a></li>
                                @endcanAccess
                            </ul>
                        </li>
                    @endcanAccess
                    @canAccess('Providers-Menu', ['View'])
                        <li><a href="#">Providers</a></li>
                    @endcanAccess
                    @canAccess('Financial-Menu', ['View'])
                    <li><a href="#">Financial</a></li>
                    @endcanAccess
                    @canAccess('CM-Menu', ['View'])
                    <li><a href="#">CM</a></li>
                    @endcanAccess
                    @canAccess('Leads-Menu', ['View'])
                    <li><a href="#">Leads</a></li>
                    @endcanAccess
                    @canAccess('Affiliates-Menu', ['View'])
                    <li><a href="#">Affiliates</a></li>
                    @endcanAccess

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                        </li>
                </ul>
            </div>
        </div>
    </nav>
@endif
