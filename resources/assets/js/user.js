import $ from 'jquery';

export default (() => {
    
    function main(){
        let body = $('body');

        body.on('click','#ibNetworkUser', (event) => {
            let element = $(event.target);
            let isChecked = element.is(':checked');

            $('.password-container').toggle(!isChecked);
            $('.password-container input').prop('required', !isChecked);
        });
    }

    if($('.user-component').length > 0){
        main();
    }
})();