#!/bin/sh

# If you would like to do some extra provisioning you may
# add any commands you wish to this file and they will
# be run after the Homestead machine is provisioned.
echo  'cd code' >> ~/.bashrc
sudo apt-get update
sudo apt-get -y install php7.2-ldap
sudo service nginx restart
cd code
composer dump-autoload -o
php artisan migrate --seed