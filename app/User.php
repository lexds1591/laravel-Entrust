<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\NoloAuthorization;

class User extends Authenticatable
{
    use Notifiable;

    use NoloAuthorization;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * This method provides relationship between tables.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userPrivileges()
    {
        return $this->hasMany(Models\RbacUserPrivilege::class, 'userId');
    }

    /**
     * This method provides relationship between tables.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function platformUser()
    {
        return $this->hasOne(Models\PlatformUser::class, 'id');
    }
}
