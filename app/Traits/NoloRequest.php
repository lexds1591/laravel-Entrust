<?php
namespace App\Traits;

/**
 * Trait NoloRequest
 * @package App\Traits
 */
trait NoloRequest{

    /**
     * @param array $checkboxes
     * @return mixed
     */
    public function allWithCheckboxes(array $checkboxes)
    {
        $checkboxes = collect($checkboxes);

        $checkboxes->each(function($checkbox){
            $value = $this->input($checkbox) ? true : false;
            $this->merge([$checkbox => $value]);
        });

        return $this->all();
    }
}