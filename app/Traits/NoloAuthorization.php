<?php
namespace App\Traits;

use Illuminate\Support\Collection;
use App\Repositories\Eloquent\RbacGroupRolePermissionRepositoryEloquent;
use App\Repositories\Eloquent\RbacRoleRepositoryEloquent;
use App\Repositories\Eloquent\RbacGroupRepositoryEloquent;
use App\Repositories\Eloquent\RbacUserPrivilegeRepositoryEloquent;
use App\Repositories\Eloquent\RbacPermissionRepositoryEloquent;

/**
 * Trait NoloAuthorization
 */
trait NoloAuthorization{

    /**
     * @return bool|mixed
     * @throws \Exception
     */
    public function attachPrivilege()
    {
        $args =  func_get_args();

        if( func_num_args() === 1 && is_integer($args[0]) ){
            return $this->createPrivilege($args[0]);
        }else if( func_num_args() === 3 ){
            if( is_integer($args[0]) && is_integer($args[1]) && $args[2] instanceof Collection ){
                return $this->attachPermissions($args[0], collect([$args[1]]), $args[2]);
            }else if( is_string($args[0]) && is_string($args[1]) && $args[2] instanceof Collection ){
                return $this->attachPermissionsByName($args[0], $args[1], $args[2]);
            }
        }

        throw new \InvalidArgumentException('Invalid parameters');
    }

    /**
     * @param string $roleName
     * @param Collection $permissions
     * @return bool
     * @throws \Exception
     */
    public function canAccess(string $roleName, Collection $permissions)
    {
        $rbacUserPrivilegeRepository = resolve(RbacUserPrivilegeRepositoryEloquent::class);

        if($this->isAdmin()){
            return true;
        }

        return (
            $rbacUserPrivilegeRepository
            ->findByRolePermissions($roleName, $permissions)
            ->count()
        ) >= 1;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function hasPrivilege()
    {
        $args =  func_get_args();

        if( func_num_args() === 1 && is_integer($args[0]) ){
            return $this->hasGroupRolePermission($args[0]);
        }else if( func_num_args() === 3 ){
            if( is_string($args[0]) && is_string($args[1]) && $args[2] instanceof Collection ){
                return $this->hasGroupRolePermissions($args[0], $args[1], $args[2]);
            }
        }

        throw new \InvalidArgumentException('Invalid parameters');
    }

    /**
     * @param int $groupId
     * @param null $userId
     * @return mixed
     */
    public function createPrivilege(int $groupId, $userId = null)
    {
        $rbacUserPrivilegeRepository = resolve(RbacUserPrivilegeRepositoryEloquent::class);

        $userId = (!is_null($userId)) ? $userId : $this->id;

        return $rbacUserPrivilegeRepository->create([
            'userId' => $userId,
            'groupId' => $groupId
        ]);
    }

    /**
     * @param int $groupId
     * @param Collection $roles
     * @param Collection $permissions
     * @return bool
     */
    public function attachPermissions(int $groupId, Collection $roles, Collection $permissions)
    {
        $rbacPermissionRepository = resolve(RbacPermissionRepositoryEloquent::class);
        $rbacGroupRolePermissionRepository = resolve(RbacGroupRolePermissionRepositoryEloquent::class);


        $roles->each(function($roleId) use($rbacPermissionRepository, $rbacGroupRolePermissionRepository, $groupId, $permissions){
            $permissions->each(function($permissionName) use ($rbacPermissionRepository, $rbacGroupRolePermissionRepository, $groupId, $roleId){
                $permission = $rbacPermissionRepository->findByField('name', $permissionName)->first();

                $rbacGroupRolePermission = $rbacGroupRolePermissionRepository->findWhere([
                    'groupId' => $groupId,
                    'roleId' => $roleId,
                    'permissionId' => $permission->id,

                ])->first();

                if(is_null($rbacGroupRolePermission)) {
                    $rbacGroupRolePermission = $rbacGroupRolePermissionRepository->create([
                        'groupId' => $groupId,
                        'roleId' => $roleId,
                        'permissionId' => $permission->id,

                    ]);
                }

                $this->createPrivilege($rbacGroupRolePermission->id);
            });
        });


        return true;
    }

    /**
     * @param string $groupName
     * @param string $roleName
     * @param Collection $permissions
     * @return bool
     * @throws \Exception
     */
    public function attachPermissionsByName(string $groupName, string $roleName, Collection $permissions)
    {
        $rbacGroupRepository = resolve(RbacGroupRepositoryEloquent::class);
        $rbacRoleRepository = resolve(RbacRoleRepositoryEloquent::class);

        $group = $rbacGroupRepository->findByField('name', $groupName)->first();

        if(is_null($group)){
            throw new \Exception($groupName . ' group does not exist');
        }

        $role = $rbacRoleRepository->findByField('name', $roleName)->first();

        if(is_null($role)){
            throw new \Exception($roleName . ' role does not exist');
        }

        return $this->attachPermissions($group->id, collect([$role->id]), $permissions);
    }

    /**
     * TODO - change admin to PlatformUser table column
     * @return mixed
     */
    public function isAdmin()
    {
        return $this->platformUser->isAdmin;
    }

    /**
     * @param string $groupName
     * @param string $roleName
     * @param Collection $permissions
     * @return bool
     * @throws \Exception
     */
    public function hasGroupRolePermissions(string $groupName, string $roleName, Collection $permissions)
    {
        $rbacUserPrivilegeRepository = resolve(RbacUserPrivilegeRepositoryEloquent::class);

        return (
            $rbacUserPrivilegeRepository
                ->findGroupRolePermissions($groupName, $roleName, $permissions)
                ->count()
        ) >= 1;
    }

    /**
     * @param int $groupRolePermissionId
     * @return bool
     * @throws \Exception
     */
    public function hasGroupRolePermission(int $groupRolePermissionId)
    {
        $rbacGroupRolePermissionRepository = resolve(RbacGroupRolePermissionRepositoryEloquent::class);

        $rbacGroupRolePermission = $rbacGroupRolePermissionRepository->findByField('id', $groupRolePermissionId)->first();

        if(is_null($rbacGroupRolePermission)){
            throw new \Exception($groupRolePermissionId . ' groupRolePermissionId does not exist');
        }

        return true;
    }
}
