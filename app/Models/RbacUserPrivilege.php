<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class RbacUserPrivilege extends Model implements Transformable
{
    use TransformableTrait;

    use SoftDeletes;

    /**
     * Table name.
     *
     *
     * @var string
     */
    protected $table = 'RbacUserPrivileges';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userId',
        'groupId',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    /**
     * Attributes not shown on responses.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'userId' => 'integer',
        'groupId' => 'integer',
    ];

    /**
     * FIXME: delete this function and use group relation
     * This method provides relationship between tables.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function groupRolePermission()
    {
        return $this->belongsTo(RbacGroupRolePermission::class, 'groupId', 'groupId');
    }

    public function group()
    {
        return $this->belongsTo(RbacGroup::class, 'groupId');
    }

    public function platformUser(){
        return $this->belongsTo(PlatformUser::class ,'userId');
    }

}
