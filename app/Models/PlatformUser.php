<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class PlatformUser extends Model implements Transformable
{
    use TransformableTrait;

    use SoftDeletes;

    /**
     * Table name.
     *
     *
     * @var string
     */
    protected $table = 'PlatformUsers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'userName',
        'firstName',
        'lastName',
        'email',
        'ibNetworkUser',
        'isAdmin',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    /**
     * Attributes not shown on responses.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'userName' => 'string',
        'firstName' => 'string',
        'lastName' => 'string',
        'email' => 'string',
        'ibNetworkUser' => 'boolean',
        'isAdmin' => 'boolean',
    ];

}
