<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\RbacRoleCreateRequest;
use App\Http\Requests\RbacRoleUpdateRequest;
use App\Contracts\Repositories\RbacRoleRepository;
use App\Validators\RbacRoleValidator;


class RbacRolesController extends Controller
{

    /**
     * @var RbacRoleRepository
     */
    protected $repository;

    /**
     * @var RbacRoleValidator
     */
    protected $validator;

    public function __construct(RbacRoleRepository $repository, RbacRoleValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $rbacRoles = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $rbacRoles,
            ]);
        }

        return view('rbacRoles.index', compact('rbacRoles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RbacRoleCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(RbacRoleCreateRequest $request)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $rbacRole = $this->repository->create($request->all());

            $response = [
                'message' => 'RbacRole created.',
                'data'    => $rbacRole->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rbacRole = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $rbacRole,
            ]);
        }

        return view('rbacRoles.show', compact('rbacRole'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $rbacRole = $this->repository->find($id);

        return view('rbacRoles.edit', compact('rbacRole'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  RbacRoleUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(RbacRoleUpdateRequest $request, $id)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $rbacRole = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'RbacRole updated.',
                'data'    => $rbacRole->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'RbacRole deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'RbacRole deleted.');
    }
}
