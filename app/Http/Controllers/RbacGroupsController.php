<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\RbacGroupCreateRequest;
use App\Http\Requests\RbacGroupUpdateRequest;
use App\Repositories\Eloquent\RbacGroupRepositoryEloquent;


class RbacGroupsController extends Controller
{

    /**
     * @var mixed
     */
    private $rbacGroupRepository;

    /**
     * UserController constructor.
     */
    function __construct()
    {
        $this->rbacGroupRepository = resolve(RbacGroupRepositoryEloquent::class);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('admin.group.create-edit', [
            'isEdit' => false,
            'isCreate' => true,
            'action' => route('admin.group.store'),
        ]);
    }

    /**
     * @param RbacGroupCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(RbacGroupCreateRequest $request)
    {
        $this->rbacGroupRepository->create($request->all());

        return redirect('/');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, $id)
    {
        $group = $this->rbacGroupRepository->find($id);

        return view('admin.group.create-edit', [
            'isEdit' => true,
            'isCreate' => false,
            'group' => $group,
            'action' => route('admin.group.update', ['id' => $id]),
            'deleteRoute' => route('admin.group.delete', ['id' => $id])
        ]);
    }

    /**
     * @param RbacGroupUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(RbacGroupUpdateRequest $request, $id)
    {
        $this->rbacGroupRepository->update($request->all(), $id);

        return redirect('/');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $this->rbacGroupRepository->delete($id);

        return redirect('/');
    }
}
