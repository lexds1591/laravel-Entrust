<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\PlatformUser;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    const LDAP_OPT_DIAGNOSTIC_MESSAGE = 0x0032;

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers{
        login as laravelLogin;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        if(is_null($request->password)){
            return $this->sendFailedLoginResponse($request);
        }

        $platformUser = PlatformUser::where('email', $request->email)
            ->orWhere('userName', $request->email)
            ->first();

        if(!is_null($platformUser) ){
            if($platformUser->ibNetworkUser) {
                $user = $this->getLDAPUser($request->email);

                if ($this->ldapLogin($user, $request->password)) {
                    Auth::login(User::find($platformUser->id));
                    return redirect($this->redirectTo);
                }
                return $this->sendFailedLoginResponse($request);
            }
        }
        else{
            return $this->sendFailedLoginResponse($request);
        }

        $request->merge([
            'email' => $platformUser->email
        ]);

        return $this->laravelLogin($request);
    }

    /**
     * @param $user
     * @param $password
     * @return bool
     * @throws \Exception
     */
    private function ldapLogin($user, $password)
    {
        $connection = ldap_connect(config('services.ldap.default_host')) or die("Could not connect to LDAP server.");

        if ($connection) {
            $this->setLdapOptions($connection);

            try {
                // binding to ldap server
                $bind = ldap_bind($connection, $user, $password);

                // verify binding
                if ($bind) {
                    // not using AD group role for permissions, dont need binding and connection to AD, release resource
                    ldap_unbind($connection);
                    return true;
                } else {
                    $this->reportError($connection);
                }
            }catch(\Exception $exception){

            }
        }

        return false;
    }

    /**
     * @param $userName
     * @return string
     */
    private function getLdapUser($userName)
    {
        $user = $userName;
        $domain = config('services.ldap.default_domain');

        return $user.'@'.$domain;
    }

    /**
     * @param $connection
     */
    private function setLdapOptions($connection)
    {
        // may need the following line to avoid warning message, could be higher version now
        ldap_set_option($connection, LDAP_OPT_PROTOCOL_VERSION, 3);

        // set timeout value for ldap bind attempt
        ldap_set_option($connection, LDAP_OPT_NETWORK_TIMEOUT, 10);
    }

    /**
     * @param $connection
     * @throws \Exception
     */
    private function reportError($connection)
    {
        if (ldap_get_option($connection, LDAP_OPT_DIAGNOSTIC_MESSAGE, $extended_error)) {
            throw new \Exception("Error Binding to LDAP: $extended_error");
        } else {
            throw new \Exception('Error Binding to LDAP: No additional information is available.');
        }
    }
}
