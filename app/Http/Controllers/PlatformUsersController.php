<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\PlatformUserCreateRequest;
use App\Http\Requests\PlatformUserUpdateRequest;
use App\Contracts\Repositories\PlatformUserRepository;
use App\Validators\PlatformUserValidator;


class PlatformUsersController extends Controller
{

    /**
     * @var PlatformUserRepository
     */
    protected $repository;

    /**
     * @var PlatformUserValidator
     */
    protected $validator;

    public function __construct(PlatformUserRepository $repository, PlatformUserValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $platformUsers = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $platformUsers,
            ]);
        }

        return view('platformUsers.index', compact('platformUsers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PlatformUserCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(PlatformUserCreateRequest $request)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $platformUser = $this->repository->create($request->all());

            $response = [
                'message' => 'PlatformUser created.',
                'data'    => $platformUser->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $platformUser = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $platformUser,
            ]);
        }

        return view('platformUsers.show', compact('platformUser'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $platformUser = $this->repository->find($id);

        return view('platformUsers.edit', compact('platformUser'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  PlatformUserUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(PlatformUserUpdateRequest $request, $id)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $platformUser = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'PlatformUser updated.',
                'data'    => $platformUser->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'PlatformUser deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'PlatformUser deleted.');
    }
}
