<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PlatformUserCreateRequest;
use App\Http\Requests\PlatformUserUpdateRequest;
use App\Repositories\Eloquent\PlatformUserRepositoryEloquent;

class UserController extends Controller
{
    /**
     * @var mixed
     */
    private $platformUserRepository;

    /**
     * UserController constructor.
     */
    function __construct()
    {
        $this->platformUserRepository = resolve(PlatformUserRepositoryEloquent::class);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('admin.user.create-edit', [
            'isEdit' => false,
            'isCreate' => true,
            'action' => route('admin.user.store')
        ]);
    }

    /**
     * @param PlatformUserCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PlatformUserCreateRequest $request)
    {
        $this->platformUserRepository->create($request->allWithCheckboxes([
            'ibNetworkUser',
            'isAdmin',
        ]));

        return redirect('/');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, $id)
    {
        $platformUser = $this->platformUserRepository->find($id);

        return view('admin.user.create-edit', [
            'isEdit' => true,
            'isCreate' => false,
            'platformUser' => $platformUser,
            'action' => route('admin.user.update', ['id' => $id]),
            'deleteRoute' => route('admin.user.delete', ['id' => $id])
        ]);
    }

    /**
     * @param PlatformUserUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(PlatformUserUpdateRequest $request, $id)
    {
        $this->platformUserRepository->update($request->allWithCheckboxes([
            'ibNetworkUser',
            'isAdmin',
        ]), $id);

        return redirect('/');
    }

    /**
     * TODO - add validation in login for platformUserDeleted
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $this->platformUserRepository->delete($id);

        return redirect('/');
    }
}
