<?php

namespace App\Http\Controllers;

use App\Repositories\Eloquent\RbacGroupRepositoryEloquent;
use App\Repositories\Eloquent\RbacGroupRolePermissionRepositoryEloquent;
use App\Repositories\Eloquent\RbacPermissionRepositoryEloquent;
use App\Repositories\Eloquent\RbacRoleRepositoryEloquent;
use App\Http\Requests\RbacGroupRolePermissionCreateRequest;
use App\Http\Requests\RbacGroupRolePermissionUpdateRequest;
use App\Contracts\Repositories\RbacGroupRolePermissionRepository;
use App\User;



class RbacGroupRolePermissionsController extends Controller
{

    /**
     * @var RbacGroupRolePermissionRepository
     */
    protected $groupRolePermissionRepository;
    protected $groupRepository;
    protected $roleRepository;
    protected $permissionRepository;

    public function __construct()
    {
        $this->groupRolePermissionRepository = resolve(RbacGroupRolePermissionRepositoryEloquent::class);
        $this->groupRepository = resolve(RbacGroupRepositoryEloquent::class);
        $this->roleRepository = resolve(RbacRoleRepositoryEloquent::class);
        $this->permissionRepository = resolve(RbacPermissionRepositoryEloquent::class);

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = $this->groupRepository->all()->toArray();
        $roles = $this->roleRepository->all()->toArray();
        $permissions = $this->permissionRepository->all()->toArray();

        $groupRolePermissionList = $this->groupRolePermissionRepository
                                        ->with(['group', 'role', 'permission'])
                                        ->all()
                                        ->toArray();

        $groupRolePermissionData = [
            'groups' => $groups,
            'roles' => $roles,
            'permissions' => $permissions,
            'groupRolePermissionList' => $groupRolePermissionList
        ];

        return view('admin.grouprolepermission.index', $groupRolePermissionData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RbacGroupRolePermissionCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(RbacGroupRolePermissionCreateRequest $request)
    {

        $roles = $request->get('roleId');
        $groupId = $request->get('groupId');
        $permissions = collect(['View']); //Permission id is always 1 or 'View' in v1.0

        //FIXME change this to use NoloAuth trait methods
        foreach ($roles as $key => $role){
            $values = [
                'groupId' => $request->get('groupId'),
                'roleId' => $role,
                'permissionId' => '1'
                ];
            $this->groupRolePermissionRepository->create($values);
        }

        /**
         *
        $user = new User();
        $user->attachPermissions($groupId, collect($roles), $permissions);
        **/
        return redirect()->route('admin.grouprolepermission.index')
            ->with('message','Record created successfully');

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     *
     */
    public function show($id)
    {

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     *
     */
    public function edit($id)
    {

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  RbacGroupRolePermissionUpdateRequest $request
     * @param  string            $id
     *
     *
     */
    public function update(RbacGroupRolePermissionUpdateRequest $request, $id)
    {


    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $this->groupRolePermissionRepository->delete($id);

        return redirect()->route('admin.grouprolepermission.index')
            ->with('message','Record deleted successfully');
    }
}
