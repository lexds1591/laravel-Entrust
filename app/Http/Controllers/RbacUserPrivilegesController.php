<?php

namespace App\Http\Controllers;

use App\Repositories\Eloquent\PlatformUserRepositoryEloquent;
use App\Repositories\Eloquent\RbacGroupRepositoryEloquent;
use App\Repositories\Eloquent\RbacGroupRolePermissionRepositoryEloquent;
use App\Repositories\Eloquent\RbacUserPrivilegeRepositoryEloquent;
use App\User;
use App\Http\Requests\RbacUserPrivilegeCreateRequest;
use App\Http\Requests\RbacUserPrivilegeUpdateRequest;



class RbacUserPrivilegesController extends Controller
{

    protected $platformUserRepository;
    protected $groupRolePermissionRepository;
    protected $userPrivilegeRepository;
    protected $groupRepository;

    public function __construct()
    {
        $this->platformUserRepository = resolve(PlatformUserRepositoryEloquent::class);
        $this->groupRolePermissionRepository = resolve(RbacGroupRolePermissionRepositoryEloquent::class);
        $this->userPrivilegeRepository = resolve(RbacUserPrivilegeRepositoryEloquent::class);
        $this->groupRepository = resolve(RbacGroupRepositoryEloquent::class);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->platformUserRepository->all();
        $groups = $this->groupRepository->all();


        $userPrivilegeList = $this->userPrivilegeRepository
            ->with(['group',
                'platformUser'])
            ->all();

        $userPrivilegeData = [
            'users' => $users,
            'groups' => $groups,
            'userPrivilegeList' => $userPrivilegeList

        ];

        return view('admin.userprivileges.index', $userPrivilegeData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RbacUserPrivilegeCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(RbacUserPrivilegeCreateRequest $request)
    {
        $user = new User();
        $user->createPrivilege($request->get('groupId'), $request->get('userId'));

        return redirect()->route('admin.userprivileges.index')
            ->with('message','Record created successfully');

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  RbacUserPrivilegeUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(RbacUserPrivilegeUpdateRequest $request, $id)
    {

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->userPrivilegeRepository->delete($id);

        return redirect()->route('admin.userprivileges.index')
            ->with('message','Record deleted successfully');
    }
}
