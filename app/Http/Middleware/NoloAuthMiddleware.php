<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;


class NoloAuthMiddleware
{

    const DELIMITER = '|';


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  $role
     * @param  $permissions
     * @return mixed
     */
    public function handle($request, Closure $next, $role, $permissions)
    {

        $userData = Auth::user();

        /**
         * If user is admin go to next request
         */
        if ($userData->isAdmin()){
            return $next($request);
        }


        $permissions = collect(explode(self::DELIMITER, $permissions));

        /**
         * Validate if the user can access to this request
         */
        if (!$userData->canAccess($role, $permissions)) {
            return abort(403, "You don't have access to this page");
        }

        return $next($request);


    }

}
