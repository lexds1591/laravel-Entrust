<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->app->bind(\App\Contracts\Repositories\PlatformUserRepository::class, \App\Repositories\Eloquent\PlatformUserRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\Repositories\RbacPermissionRepository::class, \App\Repositories\Eloquent\RbacPermissionRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\Repositories\RbacRoleRepository::class, \App\Repositories\Eloquent\RbacRoleRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\Repositories\RbacGroupRepository::class, \App\Repositories\Eloquent\RbacGroupRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\Repositories\RbacGroupRolePermissionRepository::class, \App\Repositories\Eloquent\RbacGroupRolePermissionRepositoryEloquent::class);
        $this->app->bind(\App\Contracts\Repositories\RbacUserPrivilegeRepository::class, \App\Repositories\Eloquent\RbacUserPrivilegeRepositoryEloquent::class);
        //:end-bindings:
    }
}
