<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Auth;

class DirectivesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        Blade::if('canAccess', function ($role, $permissions) {
            try {
                $canAccess = Auth::user()->canAccess($role, collect($permissions));
            }catch(\Exception $exception){
                $canAccess = false;
            };

            return $canAccess;
        });
    }
}
