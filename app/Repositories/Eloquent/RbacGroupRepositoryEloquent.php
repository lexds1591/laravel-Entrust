<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\Repositories\RbacGroupRepository;
use App\Models\RbacGroup;
use App\Validators\RbacGroupValidator;

/**
 * Class RbacGroupRepositoryEloquent
 * @package namespace App\Repositories\Eloquent;
 */
class RbacGroupRepositoryEloquent extends BaseRepository implements RbacGroupRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RbacGroup::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
