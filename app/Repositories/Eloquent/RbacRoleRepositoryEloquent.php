<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\Repositories\RbacRoleRepository;
use App\Models\RbacRole;
use App\Validators\RbacRoleValidator;

/**
 * Class RbacRoleRepositoryEloquent
 * @package namespace App\Repositories\Eloquent;
 */
class RbacRoleRepositoryEloquent extends BaseRepository implements RbacRoleRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RbacRole::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
