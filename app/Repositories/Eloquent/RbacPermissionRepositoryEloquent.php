<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\Repositories\RbacPermissionRepository;
use App\Models\RbacPermission;
use App\Validators\RbacPermissionValidator;

/**
 * Class RbacPermissionRepositoryEloquent
 * @package namespace App\Repositories\Eloquent;
 */
class RbacPermissionRepositoryEloquent extends BaseRepository implements RbacPermissionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RbacPermission::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
