<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\Repositories\PlatformUserRepository;
use App\Models\PlatformUser;
use App\User;

/**
 * Class PlatformUserRepositoryEloquent
 * @package namespace App\Repositories\Eloquent;
 */
class PlatformUserRepositoryEloquent extends BaseRepository implements PlatformUserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PlatformUser::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param array $attributes
     * @return mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function create(array $attributes)
    {
        $attributes['name'] = $attributes['firstName'].' '.$attributes['lastName'];
        $attributes['password'] = bcrypt($attributes['password']);
        $user = User::create($attributes);

        $attributes['id'] = $user->id;

        return parent::create($attributes);
    }

    /**
     * @param array $attributes
     * @param $id
     * @return mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(array $attributes, $id)
    {
        $attributes['name'] = $attributes['firstName'].' '.$attributes['lastName'];

        if(array_key_exists('password', $attributes) && !empty($attributes['password'])){
            $attributes['password'] = bcrypt($attributes['password']);
        }
        else{
            unset($attributes['password']);
        }

        $user = User::where('id', $id)->update(
            array_only($attributes, ['name', 'email', 'password'])
        );

        return parent::update($attributes, $id);
    }

}
