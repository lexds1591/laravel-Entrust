<?php

namespace App\Repositories\Eloquent;

use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\Repositories\RbacUserPrivilegeRepository;
use App\Models\RbacUserPrivilege;
use Illuminate\Support\Collection;

/**
 * Class RbacUserPrivilegeRepositoryEloquent
 * @package namespace App\Repositories\Eloquent;
 */
class RbacUserPrivilegeRepositoryEloquent extends BaseRepository implements RbacUserPrivilegeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RbacUserPrivilege::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param $role
     * @param Collection $permissions
     * @return bool
     */
    public function findByRolePermissions($role,  Collection $permissions)
    {
        $user = Auth::user();

        return $this->model->where('userId', $user->id)
            ->whereHas('groupRolePermission', function($query) use($role, $permissions){
                $query->where(function($query) use($role){
                    if(is_integer($role)){
                        $query->where('roleId', $role);
                    } else if(is_string($role)){
                        $query->whereHas('role', function($query) use($role){
                            $query->where('name', $role);
                        });
                    }
                })->whereHas('permission', function($query) use($permissions){
                    $query->whereIn('name', $permissions);
                });
            })
        ->get();
    }

    /**
     * @param $group
     * @param $role
     * @param Collection $permissions
     * @return mixed
     */
    public function findGroupRolePermissions($group, $role, Collection $permissions)
    {
        $user = Auth::user();

        return $this->model->where('userId', $user->id)
            ->whereHas('groupRolePermission', function($query) use($group, $role, $permissions){
                if(is_integer($role)){
                    $query->where('roleId', $role);
                }
                else if(is_string($role)){
                    $query->whereHas('role', function($query) use($role){
                        $query->where('name', $role);
                    });
                }

                if(is_integer($group)){
                    $query->where('groupId', $group);
                }
                else if(is_string($role)){
                    $query->whereHas('group', function($query) use($group){
                        $query->where('name', $group);
                    });
                }

                $query->whereHas('permission', function($query) use($permissions){
                    $query->whereIn('name', $permissions);
                });
            })->get();
    }
}
