<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RbacGroupRolePermissionRepository
 * @package namespace App\Contracts\Repositories;
 */
interface RbacGroupRolePermissionRepository extends RepositoryInterface
{
    //
}
