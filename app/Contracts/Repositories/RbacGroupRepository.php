<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RbacGroupRepository
 * @package namespace App\Contracts\Repositories;
 */
interface RbacGroupRepository extends RepositoryInterface
{
    //
}
