<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PlatformUserRepository
 * @package namespace App\Contracts\Repositories;
 */
interface PlatformUserRepository extends RepositoryInterface
{
    //
}
