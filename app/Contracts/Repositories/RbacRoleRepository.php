<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RbacRoleRepository
 * @package namespace App\Contracts\Repositories;
 */
interface RbacRoleRepository extends RepositoryInterface
{
    //
}
