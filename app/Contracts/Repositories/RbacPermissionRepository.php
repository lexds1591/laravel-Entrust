<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RbacPermissionRepository
 * @package namespace App\Contracts\Repositories;
 */
interface RbacPermissionRepository extends RepositoryInterface
{
    //
}
