<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RbacUserPrivilegeRepository
 * @package namespace App\Contracts\Repositories;
 */
interface RbacUserPrivilegeRepository extends RepositoryInterface
{
    //
}
